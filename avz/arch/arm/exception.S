/*
 * Copyright (C) 2014-2019 Daniel Rossier <daniel.rossier@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <config.h>

#include <asm/mmu.h>
#include <asm/processor.h>

#include <generated/asm-offsets.h>

.globl  pseudo_usr_mode
.globl	hypervisor_stack

.align	5

@ This function is called at bootstrap and
@ reboot time. It initializes some registers
ENTRY(pre_ret_to_user)
	disable_irq

	vcpu	r10
	mov 	r6, #0
	str		r6, [r10, #(OFFSET_ARCH_VCPU + OFFSET_GUEST_CONTEXT + OFFSET_HYPERVISOR_CALLBACK)]

	current_cpu r3

	@ switch to the guest stack
	ldr r0, .LChypervisor_stack
	str	sp, [r0, r3, lsl #2]

	ldr	r6,	[sp, #S_PC]		@ entry point of the guess  /  r6 is used because not altered by save_svc_context
	ldr r7, [sp, #S_PSR]
	ldr r8, [sp, #S_IP]  	@ start_info (r12)
	ldr r9, [sp, #S_R2]		@ arg (devtree/atags)

	ldr 	sp, [sp, #S_SP]   	@ get the guest stack
	sub	sp, sp, #S_FRAME_SIZE   @ will be restored later, but the contents is not important
	str	sp, [r10, #(OFFSET_ARCH_VCPU + OFFSET_GUEST_CONTEXT + OFFSET_SYS_REGS + OFFSET_VKSP)]

	@ preserve the guest stack before coming back to the hypervisor stack

	str r6, [sp, #S_PC]	@ put the entry point on the guess stack
	str r7, [sp, #S_PSR]
	str r8, [sp, #S_IP]
	str r9, [sp, #S_R2]

	@ back to hypervisor stack
	ldr r0, .LChypervisor_stack
	ldr sp, [r0, r3, lsl #2]
	ldr	r3, [sp, #S_R3]
	b restore


ENTRY(ret_to_user)

	disable_irq 					@ ensure IRQs are disabled

	bl	do_softirq

	vcpu	r10
	ldr		r11, [r10, #OFFSET_SHARED_INFO]

	@ If the softirq handling leads to trigger an interrupt in the guest,
	@ it will be processed by do_evtchn_do_upcall. The way how to
	@ process an interrupt with potentially IRQs off is under the
	@ responsibility of the guest


ENTRY(restore)

	current_cpu r11

	@ setting pseudo_usr_mode / r0, r1 re-assigned right after
	ldr 	r0, .LCpseudo_usr_mode
	mov		r1, #1
	str		r1, [r0, r11, lsl #2]


	@ restore saved registers

	ldr		r0, .LChypervisor_stack   	@ running SVC hypervisor stack
	str		sp, [r0, r11, lsl #2]

	vcpu	r10

	@ get guest stack (already stacked from save_svc_context)
	ldr		sp, [r10, #(OFFSET_ARCH_VCPU + OFFSET_GUEST_CONTEXT + OFFSET_SYS_REGS + OFFSET_VKSP)]

	ldr		r0, [sp, #S_PSR]			@ Check if return is in guest SVC or guest USR
	msr		spsr_cxsf, r0

	and		r0, r0, #PSR_MODE_MASK
	cmp		r0, #PSR_MODE_USR   			@ usr ?
	bne		restore_svc

	ldr     lr, [sp, #S_PC]!                @ Get PC

	ldmdb   sp, {r0 - lr}^                  @ Get calling r0 - lr
	mov     r0, r0
	add     sp, sp, #S_FRAME_SIZE - S_PC

	movs    pc, lr                          @ return & move spsr_svc into cpsr

restore_svc:

	ldmia	sp, {r0 - pc}^		@ load r0 - pc, cpsr


/* *********************** */

.align 5

.global prep_switch_domain
prep_switch_domain:

        @ Call, if any, the prepare_switch_xen_domain() in the guest
  	    @ Stay 8-byte aligned
        stmfd   sp!, {r0-r6, lr}
        vcpu    r0

        ldr     r1, [r0, #(OFFSET_ARCH_VCPU + OFFSET_GUEST_CONTEXT + OFFSET_PREP_SWITCH_DOMAIN_CALLBACK)]
        cmp     r1, #0

        @ r1 must be preserved

        beq     __out_prep_switch

        current_cpu r3

        ldr     r2, .LChypervisor_stack         @ running SVC hypervisor stack
        str     sp, [r2, r3, lsl #2]

        @ get guest stack (already stacked from save_svc_context)
        ldr     sp, [r0, #(OFFSET_ARCH_VCPU + OFFSET_GUEST_CONTEXT + OFFSET_SYS_REGS + OFFSET_VKSP)]

        @ setting pseudo_usr_mode / r0, r1 re-assigned right after
        ldr     r0, .LCpseudo_usr_mode
        mov     r2, #1
        str     r2, [r0, r3, lsl #2]

        adr     lr, __continue_prep_switch
        mov     pc, r1

__continue_prep_switch:

        current_cpu     r3

        ldr             r0, .LCpseudo_usr_mode
        mov             r1, #0                                  @ setting svc
        str             r1, [r0, r3, lsl #2]

        mov             r1, sp
        ldr             r2, .LChypervisor_stack @ Get the running hypervisor SVC stack
        ldr             r2, [r2, r3, lsl #2]

        mov             sp, r2

        vcpu    r0
        str             r1, [r0, #(OFFSET_ARCH_VCPU + OFFSET_GUEST_CONTEXT + OFFSET_SYS_REGS + OFFSET_VKSP)]

__out_prep_switch:
        ldmfd   sp!, {r0-r6, pc}




	.align	5


/*
 * Register switch for ARMv3 and ARMv4 processors
 * r0 = previous vcpu, r1 = previous vcpu_guest_context, r2 = next vcpu_guest_context
 * previous and next are guaranteed not to be the same.
 *
 * OFFSET_SYS_REGS = offsetof(struct vcpu_guest_context, sys_regs)
 * OFFSET_USER_REGS = offsetof(struct vcpu_guest_context, usr_regs)
 */
ENTRY(__switch_to)
	disable_irq 				@ ensure IRQs are disabled

	str		sp, [r1, #(OFFSET_SYS_REGS + OFFSET_VUSP)]  @ save hyp. stack (SVC)
	add     ip, r1, #(OFFSET_USER_REGS + OFFSET_R4)
save_ctx:
    stmia   ip, {r4 - sl, fp, ip, sp, lr}      @ Store most regs on stack

	current_cpu r6

	mrc		p15, 0, r4, c3, c0, 0
	str		r4, [r1, #(OFFSET_SYS_REGS + OFFSET_VDACR)]

load_ctx:
	ldr		r4, .LChypervisor_stack
	ldr		r5, [r2, #(OFFSET_SYS_REGS + OFFSET_VUSP)]	@ Retrieve hyp stack
	str		r5, [r4, r6, lsl #2]


	ldr		r4, [r2, #(OFFSET_SYS_REGS + OFFSET_VDACR)]
	mcr		p15, 0, r4, c3, c0, 0

	add		ip, r2, #(OFFSET_USER_REGS + OFFSET_R4)

    ldmia   ip,  {r4 - sl, fp, ip, sp, pc}       @ Load all regs saved previously

	nop
	nop
	nop

.align  5

pseudo_usr_mode:
	.space NR_CPUS * 4

@Hypervisor stack is used for the *current* (running) vcpu svc stack address
hypervisor_stack:
	.space NR_CPUS * 4


.LCpseudo_usr_mode:
	.word	pseudo_usr_mode

.LChypervisor_stack:
	.word	hypervisor_stack

