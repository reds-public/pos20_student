/*
 * Copyright (C) 2014-2018 Daniel Rossier <daniel.rossier@heig-vd.ch>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
 
/dts-v1/;

#include "skeleton.dtsi"

/ {
	model = "SO3 Pine 64";
	compatible = "arm,pine64";
	
	cpus@0 {
		device_type = "cpu";
		compatible = "arm,cortex-a15";
		reg = <0 0>;
	};

	memory@0x40000000 {
		device_type = "memory";
		reg = <0x40000000 0x10000000>; /* 256 MB */
	};

	serial@01c28000 {
		compatible ="serial,ns16550";
		reg = <0x01c28000 0x400>;
		interrupts = <32>;
		status = "ok";
	};

	/* GIC interrupt controller */
	interrupt-controller@0x01c81000 {
		compatible = "intc,gic";
		reg = <0x01c81000 0x2000>;
		interrupt-controller;
		status = "ok";
	};
	
	/* Periodic timer (sun4i) */
	periodic-timer@01C20C00 {
		compatible = "sun4i-timer,periodic-timer";
		reg = <0x01c20c00 0x20>;
		interrupts = <50>;
		status = "ok";
		};
	
};
